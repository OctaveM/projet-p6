(* open Messages *)
open Word
open Crypto


let my_concat c s = Char.escaped c ^ s

type politician = { sk : Crypto.sk; pk : Crypto.pk } [@@deriving yojson, show]

type state = {
  politician : politician;
  word_store : Store.word_store;
  mutable letter_store : Store.letter_store;
  mutable next_words : word list;
}

let rec permutations result other = function
  | [] -> [result]
  | hd :: tl ->
    let r = permutations (hd :: result) [] (other @ tl) in
    if tl <> [] then
      r @ permutations result (hd :: other) tl
    else
      r
;;
let rec sublists = function
  | []    -> [[]]
  | x::xs -> let ls = sublists xs in
               List.map (fun l -> x::l) ls @ ls
;;

let getAll list = 
  let subLists = sublists list in
  List.fold_left (fun acc ll -> acc @ permutations [] [] ll) [] subLists
;;

let letters_to_string (letters: Letter.t list) = 
  let rec aux acc w =
    match w with
      | []    -> acc
      | x::xs -> aux (my_concat (Letter.getChar x) acc) xs
  in aux "" letters

;;


let make_word_on_hash level letters politician head_hash : word =
  let head = head_hash in
  Word.make ~word:letters ~level ~pk:politician.pk ~sk:politician.sk ~head
;;

let make_word_on_blockletters level letters politician head : word =
  let head_hash = hash head in
  make_word_on_hash level letters politician head_hash
;;

let getWordFromLetters pstatus level letters = 
  let word = ref None in
  Option.iter 
    (fun head -> 
      word := Some 
        (make_word_on_blockletters
          level
          letters
          pstatus.politician
          (Word.to_bigstring head) )
    ) (Consensus.head ~level:(level - 1) pstatus.word_store);
  match !word with 
  | None   -> assert false
  | Some w -> w 
;;

let print_letters_List letters =
  let stringsLetter = letters (*List.map letters_to_string letters*) in 
  List.iter (fun x -> Log.log_info "lettre utilisee : %a@." Letter.pp x) stringsLetter
;;

let getLettersToUse pstatus level = 
  let head = Consensus.head ~level:level pstatus.word_store in
  let hash = match head with 
  | None -> Constants.genesis
  | Some h -> Word.hash h
  in 
  Store.get_letters pstatus.letter_store hash
;;



let getWords pstatus level = 
  let lettersList =  getLettersToUse pstatus level in
  print_letters_List lettersList;
  let words = getAll lettersList in
  Log.log_info "permutations generees\n";
  (* print_letters_List words; *)
  let filtered = List.filter 
    (fun w -> 
      (* Word.read_word 
        (letters_to_string w) 
        "dict/dict_100000_1_10.txt" *)
        ignore w;
        true
    ) 
    words
  in
  Log.log_info "Mots filtres\n";
  let res = List.map 
    (fun w -> getWordFromLetters pstatus (level + 1) w)
    filtered
  in
  Log.log_info "Mots generes\n";
  pstatus.next_words <- res;
  res
;;

let my_max = function
    [] -> assert false
  | x::xs -> List.fold_left max x xs
;;

let getWordFromConsensus pstatus words = 
  Log.log_info "Avant fitness\n";
  let fitnesses = List.map (Consensus.fitness pstatus.word_store) words in
  Log.log_info "apres fitness\n";
  let max = my_max fitnesses in
  let combined = List.combine fitnesses words in 
  let wordFit = List.filter  
    (fun (fit, _) -> (fit = max) )
    combined
  in 
  let word = List.map (fun (_, w) -> w) wordFit in
  Log.log_info "Consensus fait\n";
  pstatus.next_words <- word;
  List.nth word 0
;;

let send_new_word pstatus =
  let message = Messages.Inject_word (List.nth pstatus.next_words 0) in
  Client_utils.send_some message;
;;


let run ?(max_iter = 0) () =
  (* Generate public/secret keys *)
  let (pk, sk) = Crypto.genkeys () in
  Log.log_info "Cle generee\n";
  
  (* ignoring unused variables - to be removed *)
  (* end ignoring unused variables - to be removed *)

  
  (* Get initial wordpool *)
  let getpool = Messages.Get_full_wordpool in
  Client_utils.send_some getpool ;
  let wordpool =
    match Client_utils.receive () with
    | Messages.Full_wordpool wordpool -> wordpool
    | _ -> assert false
  in
  let store = Store.init_words () in
  Store.add_words store wordpool.words;
  Log.log_info "Fetched word_pool\n";


  (* Get initial letterpool *)
  let lpool = Messages.Get_full_letterpool in
  Client_utils.send_some lpool ;
  let letterpool =
    match Client_utils.receive () with
    | Messages.Full_letterpool letterpool -> letterpool
    | _ -> assert false
  in
  let letter_store = Store.init_letters () in
  Store.add_letters letter_store letterpool.letters;
  Log.log_info "Fetched letters\n";


  let pstatus: state = {
    politician = {pk = pk; sk = sk};
    word_store = store; (* les mots retenus par le consensus *) 
    letter_store = letter_store;
    next_words = [] (* les mots genere (utile pour le mode roue libre) *) 
  } in

  (* Create and send first word *)
  let words = getWords pstatus (wordpool.current_period - 1) in 
  let word = getWordFromConsensus pstatus words in 
  ignore word;
  
  send_new_word pstatus;
  (* start listening to server messages *)
  Client_utils.send_some Messages.Listen ;
  (* start main loop *)
  let level = ref wordpool.current_period in
  let rec loop max_iter =
    if max_iter = 0 then ()
    else (
      ( match Client_utils.receive () with
        | Messages.Inject_letter l -> Store.add_letter pstatus.letter_store l
        | Messages.Next_turn p -> level := p ;
                (* pstatus.letter_store <- (Store.init_letters () ); *)
                let lpool = Messages.Get_wordpool_since (!level - 1) in
                Client_utils.send_some lpool; ()
        | Messages.Diff_wordpool { since = _; wordpool = wordpool } -> 
              let (_, word) = (List.nth wordpool.words 0) in 
              Store.add_word (pstatus.word_store) word; (* Add the first word*)
              let words = getWords pstatus (wordpool.current_period - 1) in 
              let _ = getWordFromConsensus pstatus words in  
              send_new_word pstatus
              (* Lorsqu'un mot a ete choisi par consensus *)
        | _ -> ()
        
        );
      loop (max_iter - 1) 
    )
  in
  loop max_iter
;;

let _ =
  let main =
    Random.self_init () ;
    let () = Client_utils.connect () in
    run ~max_iter:(-1) ()
  in
  main
