open Word
open Constants


(* ignoring unused variables - to be removed *)
let _ = ignore genesis

(* end ignoring unused variables - to be removed *)

let letter_score letter =
  match (Letter.getChar letter) with 
  | 'k' | 'x' | 'w' | 'y' | 'z' -> 10
  | 'j' | 'q' -> 8
  | 'f' | 'h' | 'v' -> 4
  | 'b' | 'c' | 'p' -> 3
  | 'd' | 'g' | 'm' -> 2
  | _  -> 1
;;
let word_score { word; _ } : int =
  List.fold_left (fun acc el -> acc + (letter_score el)) 0 word
;;
let fitness st word =
  ignore st ;
  word_score word
;;

let head ?level (st : Store.word_store) =
  match level with 
  | None -> Some Constants.genesis_word
  | Some a -> 
    if a < 2 then Some Constants.genesis_word else 
      let mot = ref None in
      Store.iter_words (fun _ b -> if b.level = a then mot := Some b) st;
      !mot
