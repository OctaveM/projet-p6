let rec permutations result other = function
  | [] -> [result]
  | hd :: tl ->
    let r = permutations (hd :: result) [] (other @ tl) in
    if tl <> [] then
      r @ permutations result (hd :: other) tl
    else
      r
;;
let rec sublists = function
  | []    -> [[]]
  | x::xs -> let ls = sublists xs in
               List.map (fun l -> x::l) ls @ ls
;;

let sort_and_remove_duplicates l = 
  let sl = List.sort compare l in
  let rec go l acc = match l with
    | [] -> List.rev acc
    | [x] -> List.rev (x::acc) 
    | (x1::x2::xs) -> 
      if x1 = x2
      then go (x2::xs) acc
      else go (x2::xs) (x1::acc)
  in go sl []
;;
let getAll list = 
  let res = ref [] in 
  let subLists = sublists list in
  List.fold_left (fun acc ll -> acc @ permutations [] [] ll) [] subLists
;;
